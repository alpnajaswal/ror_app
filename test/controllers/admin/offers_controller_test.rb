require 'test_helper'

class Admin::OffersControllerTest < ActionController::TestCase
  setup do
    @offer = offers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:offers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create offer" do
    assert_difference('Offer.count') do
      post :create, offer: { description: @offer.description, offer_prize: @offer.offer_prize, offer_type: @offer.offer_type, points: @offer.points, status: @offer.status, title: @offer.title }
    end

    assert_redirected_to admin_offer_path(assigns(:offer))
  end

  test "should show offer" do
    get :show, id: @offer
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @offer
    assert_response :success
  end

  test "should update offer" do
    patch :update, id: @offer, offer: { description: @offer.description, offer_prize: @offer.offer_prize, offer_type: @offer.offer_type, points: @offer.points, status: @offer.status, title: @offer.title }
    assert_redirected_to admin_offer_path(assigns(:offer))
  end

  test "should destroy offer" do
    assert_difference('Offer.count', -1) do
      delete :destroy, id: @offer
    end

    assert_redirected_to admin_offers_path
  end
end
