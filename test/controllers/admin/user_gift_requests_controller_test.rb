require 'test_helper'

class Admin::UserGiftRequestsControllerTest < ActionController::TestCase
  setup do
    @user_gift_request = user_gift_requests(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:user_gift_requests)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create user_gift_request" do
    assert_difference('UserGiftRequest.count') do
      post :create, user_gift_request: { created_at: @user_gift_request.created_at, id: @user_gift_request.id, offer_id: @user_gift_request.offer_id, status: @user_gift_request.status, user_id: @user_gift_request.user_id }
    end

    assert_redirected_to admin_user_gift_request_path(assigns(:user_gift_request))
  end

  test "should show user_gift_request" do
    get :show, id: @user_gift_request
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @user_gift_request
    assert_response :success
  end

  test "should update user_gift_request" do
    patch :update, id: @user_gift_request, user_gift_request: { created_at: @user_gift_request.created_at, id: @user_gift_request.id, offer_id: @user_gift_request.offer_id, status: @user_gift_request.status, user_id: @user_gift_request.user_id }
    assert_redirected_to admin_user_gift_request_path(assigns(:user_gift_request))
  end

  test "should destroy user_gift_request" do
    assert_difference('UserGiftRequest.count', -1) do
      delete :destroy, id: @user_gift_request
    end

    assert_redirected_to admin_user_gift_requests_path
  end
end
