require 'test_helper'

class Admin::PointInfosControllerTest < ActionController::TestCase
  setup do
    @point_info = point_infos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:point_infos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create point_info" do
    assert_difference('PointInfo.count') do
      post :create, point_info: { description: @point_info.description, status: @point_info.status, title: @point_info.title }
    end

    assert_redirected_to admin_point_info_path(assigns(:point_info))
  end

  test "should show point_info" do
    get :show, id: @point_info
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @point_info
    assert_response :success
  end

  test "should update point_info" do
    patch :update, id: @point_info, point_info: { description: @point_info.description, status: @point_info.status, title: @point_info.title }
    assert_redirected_to admin_point_info_path(assigns(:point_info))
  end

  test "should destroy point_info" do
    assert_difference('PointInfo.count', -1) do
      delete :destroy, id: @point_info
    end

    assert_redirected_to admin_point_infos_path
  end
end
