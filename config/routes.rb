Rails.application.routes.draw do




  namespace :admin do
    resources :point_infos
  end
  namespace :admin do
  resources :user_gift_requests do
     member do
     get "gift_request_send"
     end
  end
  end
  namespace :admin do
    resources :offers
  end
	root "common#index"


# resources :albums do
# 	member do
# 		get 'album_songs'
# 	end
# end
    resources :subscribe_news do
    collection do
    	post 'subscribe_me' => "subscribe_news#subscribe_me"
    end
   end
    resources :notifications do
    	collection do
         get 'unread_notification' => "notifications#unread_notification"
         get 'notification_list' => "notifications#notification_list"
         post 'update_notification' => "notifications#update_notification"

       end
   end
	resources :albums do
     member do
      	get 'album_songs' => "albums#republic"
      end
      collection do
	   get 'add_album' => "albums#add_album"
	   post 'create_album' => "albums#create"
	   get 'albums' => "albums#index"
	   

     end
   end
   resources :genres , except: [:show]  do
   	collection do
   	get 'upload_song' => "genres#upload_song"
   end
   end
    resources :songs 
     post 'add_song'=>"songs#create"

    resources :user
	get 'profile' => "users#profile"
	put "update_password" => "users#update_password"
    
	get 'usersetting' => "users#usersetting"
	get 'my_profile' => "users#my_profile"
	patch 'update_profile' => "users#update_profile"
	get 'notifications' => "notifications#notifications"
	get 'setting' => "notifications#setting"
  
	get 'forgetpassword' => "common#forgetpassword"
     post "social_login" => "common#social_login"
	get 'contact_us' => "common#contact_us"
	get 'about_us' => "common#about_us"
	get 'privacy' => "common#privacy"
	get 'how_it_work' => "common#how_it_work"
	

	get  "common/check_email"
 



  namespace :admin do
    resources :genres
  end
  namespace :admin do
    resources :users
    
    get "/" => "users#dashboard"
    
    get "/change_password" => "users#change_password"
    get "/update_password" => "users#update_password"
    get  "/check_email"   => "users#check_email"
 
    
  end

   get "set_password" => "common#set_password"
   put "updatepassword" => "common#update_password"
   
  
# devise_for :users, controllers: {
#   sessions: 'users/sessions'
# }

   devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

	namespace :api, defaults: { format: :json } do
		namespace :v1 do
			resources :users, only: [:index] do
				collection do
				  get "me"
				  post "sign_in"
				  get "sign_out"
				  post "sign_up"
				  post "update"
				  get "recent_music"
				  get "followings"
				  get "followers"
				  post "review"
				  get "reviews"
				  post "select_genre"
				  get "common_genre"
				  post "forgot_password"
				  post "varify_otp" 
				  post "varify_forgot_otp" 
				  post "update_password" 
				  post "change_current_password" 
				  get "get_points"
				  get "get_offers"
				   post "resend_otp"
				   post "browse"
				   post "social_login"
				   post "setting_notification"
				   get "all_notification"
				   get "unread_notification"
				   
				end
				member do
				  get "profile"
				  get "add_new_point"
                 end
				  get "follow"
				  get "unfollow"
				  get "remove_from_favorite"
				  get "add_to_favourite"
			end
			resources :user_playlists do
				get "playlist_songs"
				post "add_song"
				get  "delete"

			end
			 resources :offers do
			    	 member do
			      	get 'redeem_offer' => "offer#redeem_offer"
			      end
              end
			resources :genres, only: [:index] do
				resources :albums, only:[:index,:show]
				collection do
				  get "all"
				 end
			end
			 
			
			
            resources :songs, only: [:play] do
                member do
				  get "play"
				  get "full_play"
				  
                end
				 collection do
				 	 get "recent_play_list"
				 	 get "all_songs"
				 	 
				 end
				  post "review"
				  get "reviews"
				  get "remove_from_favorite"
				  get "add_to_favourite"
            end
		end
	end

end
