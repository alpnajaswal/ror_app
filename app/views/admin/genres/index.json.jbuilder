json.array!(@genres) do |genre|
  json.extract! genre, :name, :image, :status
  json.url genre_url(genre, format: :json)
end