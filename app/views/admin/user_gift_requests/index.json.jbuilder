json.array!(@user_gift_requests) do |user_gift_request|
  json.extract! user_gift_request, :id, :user_id, :offer_id, :created_at, :status
  json.url user_gift_request_url(user_gift_request, format: :json)
end