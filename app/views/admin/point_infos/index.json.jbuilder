json.array!(@point_infos) do |point_info|
  json.extract! point_info, :title, :description, :status
  json.url point_info_url(point_info, format: :json)
end