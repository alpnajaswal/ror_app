json.array!(@users) do |user|
  json.extract! user, :first_name, :last_name, :image, :email, :city, :state, :mobile, :status
  json.url user_url(user, format: :json)
end