json.array!(@offers) do |offer|
  json.extract! offer, :title, :description, :offer_prize, :points, :offer_type, :status
  json.url offer_url(offer, format: :json)
end