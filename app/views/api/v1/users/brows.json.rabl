object false

unless @album.present?
	node :album do
	   []
	end
else
	child  @album.order("id desc")  ,:root => "album" ,:object_root => false do
	attributes :id,:image,:title
       end
end
unless @song.present?
	node :song do
	[]
	end
else
	child  @song.order("id desc")  ,:root => "song" ,:object_root => false do
		node(:is_favourite) {|song| song.is_favourite?(@current_api_user)}
		attributes :id ,:title,:file,:description,:duration
			child :album,:root => "album" ,:object_root => false  do 
			attributes :id ,:title,:image,:user_id,:artist_name
			end
   end
end
unless @artist.present?
	node :artist do
	[]
	end
else
	child @artist.order("id desc"),:root => "artist" ,:object_root => false  do 
	attributes :id ,:first_name,:last_name,:username
	end
end
unless @genre.present?
	node :genre do
	[]
end
else

	child @genre.order("id desc"),:root => "genre" ,:object_root => false  do 
	attributes :id ,:name,:image,:icon_image
	end
end
