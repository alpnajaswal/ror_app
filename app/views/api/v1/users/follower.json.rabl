object false
unless @followers.present?
	node :followers do
			[]
		end
else
	child  @followers,:root => "followers" ,:object_root => false do
		attributes :id,:first_name,:last_name,:image_url,:total_points_earn
      node(:is_follow) {|other_user| other_user.is_follow?(@current_api_user)}
	   

	end
end