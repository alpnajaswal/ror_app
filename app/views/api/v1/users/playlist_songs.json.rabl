
object false
unless @playlist_songs.present?
	node :playlist_songs do
			[]
		end
else
	child  @playlist_songs.order("id desc")  ,:root => "playlist_songs" ,:object_root => false do
			attributes :id,:created_at,:song
	end
end
