object false
unless @user.present?
	node :users do
			[]
		end
else

	child  @user,:root => "user" ,:object_root => false do
		attributes :id,:first_name,:last_name,:image_url,:age,:dob,:city,:state,:username
        node(:is_favourite) {|other_user| other_user.is_favourite?(@current_api_user)}
		node(:following) {@user.following_users.count}
		node(:follower) {@user.user_followers.count}
	    node(:is_follow) {|other_user| other_user.is_follow?(@current_api_user)}
		if @user.role == "U"
              if @user.points.where(:redeem=>false).present?
			node(:point) {  @user.total_points}
             else
             	node(:point) {0}
             end
           if @user.user_genre.present?
		 	child Genre.where(:id=>@user.user_genre.genre_id),:root => "genres" ,:object_root => false do
            attributes :id,:name
	        end
	      else
	    	node :genres do
			[]
		    end
	     end

	      if  @user.reviews.present?
	       child  @user.reviews.order("id desc"),:root => "my_latest_review" ,:object_root => false do
             attributes :id,:comment,:created_at
              child :reviewer,:root => "artist_detail" ,:object_root => false do
             attributes :id,:first_name,:last_name,:image,:username
             end
	        end
	    else
           node :my_latest_review do
           	[]
			# [{id:"",comment: "",artist_detail:""}]
		    end
	    end
	  else
        if  @user.my_reviews.present?
	       child  @user.my_reviews.order("id desc"),:root => "my_latest_review" ,:object_root => false do
             attributes :id,:comment,:created_at
              child :reviewable,:root => "user_detail" ,:object_root => false do
             attributes :id,:first_name,:last_name,:image,:username
             end
         end
     else
      node :my_latest_review do
           	[]
			# [{id:"",comment: "",artist_detail:""}]
		    end
     end
	  	 if @user.genres.present?
		child @user.genres.group(:id),:root => "genres" ,:object_root => false do
			attributes :id,:name
			end
		else
			 node :genres do
			[]
			end
		end
		# node(:instagram) {@user.user_social_links.where(:url_type=>"instagram").last.try(:image_url)}
		node(:instagram) {@user.instagram_url}
		node(:facebook) {@user.facebook_url}
		node(:twitter) {@user.twitter_url}
		node(:spotify) {@user.spotify_url}
		node(:itune) {@user.itune_url}
		node(:soundcloud) {@user.soundcloud_url}
		#  if @user.user_social_links.present?
		# child @user.user_social_links.group(:id),:root => "social_links" ,:object_root => false do
  #         attributes :id,:url_type,:url
  #         end
  #          else
	 #    	node :social_links do
		# 	[]
		#     end
		
	    # end
	end
	end


end