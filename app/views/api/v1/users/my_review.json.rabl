
object false
unless @current_api_user.present?
	node :user do
			[]
		end
else

	child  @current_api_user,:root => "user" ,:object_root => false do
		attributes :api_secret,:id,:email,:first_name,:last_name,:image_url,:mobile,:count_user_followers,:following_user_count,:total_review_count,:liked_artist,:city,:state
         unless @current_api_user.user_genre.present? 
         	node :genres do
			[]
		  end
		else
		  child Genre.where(:id=>@current_api_user.user_genre.genre_id),:root => "genres" ,:object_root => false do
          attributes :id,:name
          end
	    end
        node(:following) {@current_api_user.following_users.count}
		node(:follower) {@current_api_user.user_followers.count}
		  unless @current_api_user.reviews.present? 
         	node :my_latest_review do
			[]
		  end
		else
		 child @current_api_user.reviews.order("id desc"),:root => "my_latest_review" ,:object_root => false do
             attributes :id,:comment,:created_at
              child :reviewer,:root => "artist_detail" ,:object_root => false do
             attributes :id,:first_name,:last_name,:image,:username
             end
	    
	      end
		end
	end


end