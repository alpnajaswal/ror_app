object false
unless @current_api_user.user_playlists.present?
	node :playlists do
			[]
		end
else
	child  @current_api_user.user_playlists.order("id desc")  ,:root => "playlists" ,:object_root => false do
		attributes :id,:name,:total_songs
	end
end