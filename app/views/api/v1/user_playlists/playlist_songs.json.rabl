object false
unless @playlist_songs.present?
	node :playlist_songs do
			[]
		end
else
	child  @playlist_songs.order("id desc")  ,:root => "playlist_songs" ,:object_root => false do
			attributes :id,:name,:created_at

			child :song,:root => "song" ,:object_root => false  do
			node(:is_favourite) {|song| song.is_favourite?(@current_api_user)} 
						attributes :id ,:title,:file,:description
						child :album,:root => "album" ,:object_root => false  do 
						attributes :id ,:title,:image,:user_id,:artist_name
	                 end
					end
	end
end
