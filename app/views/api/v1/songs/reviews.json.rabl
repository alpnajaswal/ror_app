object false
unless @song.reviews.present?
	node :reviews do
			[]
		end
else
	child  @song.reviews.order("id desc")  ,:root => "reviews" ,:object_root => false do
		attributes :id,:comment,:created_at,:reviewer_detail
	end
end