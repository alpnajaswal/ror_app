# collection @recentsong
# attributes :api_secret,:id,:title,:file,:description,:album_id

object false
unless @recentsong.present?
	node :recent_song do
		[]
	end
else
	child @recentsong, :root => "recent", :object_root => false do
		attributes :id,:image,:title
		child :song,:root => "song" ,:object_root => false  do 
		node(:is_favourite) {|song| song.is_favourite?(@current_api_user)}
			attributes :id ,:title,:file,:description
		
		child :album,:root => "album" ,:object_root => false  do 
			attributes :id ,:title,:image,:user_id,:artist_name
			end
		end
	end
end