# collection @recentsong
# attributes :api_secret,:id,:title,:file,:description,:album_id
object false
unless @recentsong.present?
	node :recent_song do
			[]
		end
else
	


child @recentsong, :root => "recent", :object_root => false do
	attributes :id,:image,:title
	child :song,:root => "song" ,:object_root => false  do 
						attributes :id ,:title,:file,:description
	child :album,:root => "album" ,:object_root => false  do 
						attributes :id ,:title,:image,:artist_name
	end
  end
end
end