# collection @song
# attributes :id, :album_id, :title, :file, :description
object false
unless @album.present?
	node :album do
			[]
		end
else
	


child @album, :root => "Album", :object_root => false do
	attributes :id,:image,:title
	child :active_song,:root => "songs" ,:object_root => false  do 
		node(:is_favourite) {|song| song.is_favourite?(@current_api_user)}
						attributes :id ,:title,:file,:description,:duration
	    child :album,:root => "song_album" ,:object_root => false  do 
			 attributes :id ,:title,:image,:user_id,:artist_name
			   end
	end
	child :user,:root => "artist" ,:object_root => false  do 
		attributes :id ,:first_name,:last_name,:username
	end
end
end