class NewsLetterMailer < ApplicationMailer
    default from: 'no-reply@tunerewardsapp.com'
	def news_letter_mailer(details)
     @message = details[:message]
      email = details[:email]
      mail(to: email, subject: 'Tune Reward News')
   
  end
end
