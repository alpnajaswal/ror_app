class UserMailer < ApplicationMailer
	default from: 'no-reply@tunerewardsapp.com'
	 def forgot_password_mailer(details)
     @user_name = details[:user_name]
    @url  = "#{details[:base_url]}/set_password?token=#{details["token"]}"
    mail(to: details[:user_email], subject: 'Tune Reward App-Reset password instructions')
  end
def news_letter_mailer(details)
     @message = details[:message]
      email = details[:email]
      @user_name = details[:email]
      mail(to: email, subject: 'Tune Reward News')
   
  end
  def subscribe_mailer(details)
     @message = details[:message]
      email = details[:email]
      @user_name = details[:email]
      mail(to: email, subject: 'Tune Reward Subscription')
   
  end
  
end
