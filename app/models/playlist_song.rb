class PlaylistSong < ApplicationRecord
  acts_as_paranoid
  belongs_to :user_playlist
  belongs_to :song
  validates_uniqueness_of :song_id, :scope => [:song_id,:user_playlist_id]
end

