class Album < ApplicationRecord
  belongs_to :user
  belongs_to :genre,optional: true
  has_many :songs,dependent: :destroy
  mount_uploader :image, ImageUploader
  mount_base64_uploader :image, ImageUploader
   def artist_name
  	return user.artist_full_name
   end
   def active_song
   return self.songs.where(:is_submit=>1) 
   end
   def prior_friday(date)
   days_before = (date.wday + 1) % 7 + 1
  date.to_date - days_before
  end
end
