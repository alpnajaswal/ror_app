class Genre < ApplicationRecord
	has_many :albums,dependent: :destroy
	has_many :user_genres,dependent: :destroy
	has_many :users, through: :albums,dependent: :destroy
	 mount_uploader :image, GenreUploader
	 mount_base64_uploader :image, GenreUploader
	  mount_uploader :icon_image, GenreUploader
	 mount_base64_uploader :icon_image, GenreUploader
end
