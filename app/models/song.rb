class Song < ApplicationRecord
  acts_as_paranoid

  belongs_to :album
  has_many :playlist_songs, :dependent => :destroy
  has_many :recently_plays, :dependent => :destroy
  has_many :favourites, as: :favourable, :dependent => :destroy
  has_many :reviews, as: :reviewable, :dependent => :destroy
  mount_uploader :file, GenreUploader
  def check_review_add_point(current_user)
    
    if Review.where(:redeem_point => false,:reviewable_type=>"Song",:reviewer_id=>current_user.id).group("reviewable_id").count.count > 0
      Review.where(:redeem_point => false,:reviewable_type=>"Song",:reviewer_id=>current_user.id).update(:redeem_point => true)
      current_user.points.create(:points => 1,:point_type=>"song review")
       if current_user.total_points.nil?
          current_user.update(:total_points=>1)
        else
          pp = current_user.total_points+1
           current_user.update(:total_points=>pp)
        end
    if current_user.point_notification==true
        hsh = HashWithIndifferentAccess.new
        hsh[:title] = "Point Earned"
        hsh[:message] = "Hooray you have earned 1 point."
        hsh[:notification_type] = "point_earned"
        hsh[:notification_type_id] = self.id
       # hsh[:notification_count] = current_user.notifications.where(:read=>false).count
     end
       album.user.send_notification(hsh)
     end
      end
       def is_favourite?(current_user)
      return true if self.favourites.find_by_user_id(current_user.id)
      return false
    end
 # def self.refresh_songs
 #      g = Genre.all.map(&:id)
 #      all.where(:is_submit => 1).map(&:destroy)
 #      g.each do |id|
 #      @a = all.joins(:album).where('songs.is_submit'=>0).where("albums.genre_id"=>id)
 #      puts "songs ===== #{@a.count}"
 #       if @a.count<10
 #         @a.update_all(:is_submit=>1)
 #       else
 #          remaing_song = 10
 #          ary_alumn = []
 #          song = all.joins(:album).where('songs.is_submit'=>0).order("id asc").first
 #         if song.present?
 #          alumns = song.add_alumn(remaing_song,ary_alumn)
 #           puts albums
 #          end
         
 #       end
 #     end
    
     
 #    end

  # def add_alumn(remaing_song,ary_alumn)
  #         album_song_count = self.album.songs.count 
  #         if album_song_count < remaing_song
  #           ary_alumn.push(self.album)
  #           remaing_song = remaing_song - (self.album.songs.count)
  #         end
  #       song = Song.joins(:album).where('songs.is_submit'=>0).order("id asc").first
  #       if song.present?
  #         return song.add_alumn(remaing_song,ary_alumn)
  #       else
  #         return ary_alumn
  #       end


  # end
#  def self.test_data
#   alumns =  Album.all.where(:genre_id=>1)
#  (0..5000).to_a.each_with_index do |a,index| 

# Song.create(:album_id => alumns.sample.try(:id) , :title => "title#{index}", :description => "description#{index}", :created_at => (Time.now + index.minutes ))
# end  
#  end

end
