class UserPlaylist < ApplicationRecord
  belongs_to :user
  has_many :playlist_songs, dependent: :destroy
  # validates :name, :uniqueness => {:case_sensitive => false}
  def total_songs
  	return playlist_songs.count
  end
end
