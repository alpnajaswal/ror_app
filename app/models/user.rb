class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
    mount_uploader :image, ImageUploader
    mount_base64_uploader :image, ImageUploader
    has_many :recently_plays,dependent: :destroy
    has_one :user_genre, dependent: :destroy
    has_many :user_playlists,dependent: :destroy
    has_many :notifications,dependent: :destroy
    has_many :albums,dependent: :destroy
    has_many :points,dependent: :destroy
    has_many :genres, through: :albums
 
    has_many :songs, through: :albums
    has_many :user_gift_requests,dependent: :destroy
   
    has_many :user_social_links,dependent: :destroy
    has_many :reviews, as: :reviewable,dependent: :destroy
    has_many :my_reviews, :class_name => "Review", :foreign_key => "reviewer_id"
    has_many :favourites, as: :favourable,dependent: :destroy
    has_many :me_favourites, class_name: "Favourite", :foreign_key => "user_id"
   
    acts_as_followable
    acts_as_follower
    def create_token
      random_token = SecureRandom.hex(10)
      check=User.find_by_api_secret(random_token)
       if check.present?
         return create_token
       else
         self.update_attributes(:api_secret=> random_token)
       end
    end
    def self.connect_with_facebook(access_token)

     response =  HTTParty.get("https://graph.facebook.com/me",
        query: {
        access_token: access_token,
        fields: "id,picture,first_name,last_name,email"
      }).parsed_response.with_indifferent_access
      return response
    
  end
    def recent_music 
     return self.recently_plays
    end
  def my_latest_review
      latest = self.reviews
      hsh = {}
      return hsh if latest.blank?
      latest.each do |la|
      hsh[:id] = la.id
      hsh[:image] = la.reviewer.image
      hsh[:first_name] = la.reviewer.first_name
      hsh[:comment] = la.comment  
      hsh[:created_at] = la.created_at
     end
      return hsh
    end
 
    def total_review_count
      return reviews.count
     end
    def full_name
    return first_name+' '+last_name
   end
 def artist_full_name
    return username
   end
    def reviewer

    end

    def image_url
      if self.image.url.present?
        return image.url
      else
        return ""
      end
    end
    def total_points_earn
      if self.total_points.present?
        return total_points
      else
        return 0
      end
    end
    def common_genre
      puts "common_genre"

        user_ids = Album.where(:genre_id=>self.user_genre.genre_id).map(&:user_id).uniq
          main_ids = user_ids - [self.id]
          
       return User.where(id: main_ids).where(:role=>"A").where(:id => Favourite.where(:favourable_type => "User").group(:user_id).order('count_id DESC').count(:id).keys ).limit(10)
    end
    def user_recent_music
       song_ids = self.recently_plays.order("updated_at desc").limit(30).map(&:song_id).uniq
       return Song.where(id: song_ids)
    end
  def liked_artist
       user_ids = self.favourites.where(favourable_type: 'User').order("id desc").map(&:user_id).uniq
      
      if User.where(id: user_ids).present?
         user_arr = []
       User.where(id: user_ids).each do |a| 
        user_arr.push a.user_limited_info 
      end
      else
      # user_arr = HashWithIndifferentAccess.new
       user_arr = []
      # user_arr[:id] = ""
      # user_arr[:first_name] = ""
      # user_arr[:last_name] = ""
      # user_arr[:image_url] = ""
      end
      return user_arr
    end
    def user_limited_info
      
      hsh = HashWithIndifferentAccess.new
      hsh[:id] = id
      hsh[:first_name] = first_name
      hsh[:last_name] = last_name
      hsh[:image_url] = image_url
      return hsh
    end
    def is_favourite?(current_user)
      return true if current_user.favourites.find_by_user_id(self.id)
      return false
    end
    def is_follow?(current_user)
      
      return true if current_user.follows.find_by_followable_id(self.id)
      return false
    end
    def self.search(text)
    where("first_name like ? OR last_name  like ? OR username  like ? OR email like ?","%#{text}%","%#{text}%","%#{text}%","%#{text}%")
  end
   def recent_album
      return Album.order("id desc")
    end
    def send_notification(hsh)
       notifications.create!(:notification_type => hsh[:notification_type], :notification_type_id => hsh[:notification_type_id], :title => hsh[:title], :message => hsh[:message])
      # if device_type == "android" 
      #   notification_android(hsh)
      # elsif device_type == "I" 
        notification_ios(hsh)
      # end
    # else
     
    end

    def notification_android(hsh)
     
    require 'fcm'

    fcm = FCM.new("AIzaSyBqCbmj3VEvfX-kO9JGN4-jdL3IhqT_dfs")
    registration_ids= ["#{device_id}"] # an array of one or more client registration tokens # an array of one or more client registration tokens
    options = { data: {wasTapped: true, title: hsh[:title], text_message: hsh[:message], :notification_type => hsh[:notification_type], :notification_type_id => hsh[:notification_type_id]}, priority: "high", notification: {}}
    response = fcm.send(registration_ids, options)
    notifications.create!(:notification_type => hsh[:notification_type], :notification_type_id => hsh[:notification_type_id], :title => hsh[:title], :message => hsh[:message])
  end
 def send_notification_by_admin(hsh,reciever)

      if reciever.device_type == "A" 
        notification_android(hsh)
      elsif reciever.device_type == "I" 
      require 'fcm'
    fcm = FCM.new("AIzaSyAItvRZx0M6A8A7nKDR9wRJS-Wel3zwnLM")
    
        registration_ids= ["#{reciever.device_id}"] # an array of one or more client registration tokens # an array of one or more client registration tokens
     notifications.create!(:notification_type => hsh[:notification_type], :notification_type_id => hsh[:notification_type_id], :title => hsh[:title], :message => hsh[:message])
    options = { data: {wasTapped: true, title: hsh[:title], text_message: hsh[:message], :notification_type => hsh[:notification_type], :notification_type_id => hsh[:notification_type_id],:notification_count => reciever.notifications.where(:read=>false).count }, priority: "high", notification: {title: hsh[:title], body: hsh[:message], sound: "default", badge: 1, icon: "", click_action:"true",wasTapped: true}}
    response = fcm.send(registration_ids, options)
   
      end
    # else
      notifications.create!(:notification_type => hsh[:notification_type], :notification_type_id => hsh[:notification_type_id], :title => hsh[:title], :message => hsh[:message])
    end
  #Notification send ios
  def notification_ios(hsh)
     puts "send notification"
    require 'fcm'
    fcm = FCM.new("AIzaSyAItvRZx0M6A8A7nKDR9wRJS-Wel3zwnLM")
    
    # registration_ids= ["dce_onMGFjU:APA91bErxNpNnJtLG4SO3ZSXrbMgJ_iFYDhluiTf0PrZNv73_0ODCD6_zrVhcRAifii8vhPfpZkatelzWpfGckEJE2-R8eK4sYtBVwqtS5ZaCBoqkhkIQ1Z-uf2DXAdj5z7ChSiXUNzEQ-FtGuPxs8db94CiYz8gjA"] 
      registration_ids= ["#{device_id}"] # an array of one or more client registration tokens # an array of one or more client registration tokens

    

    options = { data: {wasTapped: true, title: hsh[:title], text_message: hsh[:message], :notification_type => hsh[:notification_type], :notification_type_id => hsh[:notification_type_id],:notification_count => self.notifications.where(:read=>false).count }, priority: "high", notification: {title: hsh[:title], body: hsh[:message], sound: "default", badge: 1, icon: "", click_action:"true",wasTapped: true}}
    response = fcm.send(registration_ids, options)
    

  end 
  def add_new_point
   if self.total_points.nil?
          self.update(:total_points=>1)
        else
          pp = self.total_points+1
           self.update(:total_points=>pp)
        end
        
  end

  def check_review_add_point
   # points calculate 
    # if reviews.where(:redeem_point => false,:reviewable_type => "User").group('reviewer_id').count.count > 9
        if reviews.where(:redeem_point => false,:reviewable_type => "User").group('reviewer_id').count.count == 1
         if self.total_points.nil?
          self.update(:total_points=>1)
        else
          pp = self.total_points+1
           self.update(:total_points=>pp)
        end
    
      reviews.update(:redeem_point => true)
      points.create(:points => 1,:point_type=>"user review")
        # receiver = user
        hsh = HashWithIndifferentAccess.new
        hsh[:title] = "Point Earned"
        hsh[:message] = "Hooray you have earned 1 point."
        hsh[:notification_type] = "point_earned"
         hsh[:notification_type_id] = self.id
       send_notification(hsh)
     end
      end
      def send_otp

        require 'twilio-ruby'
         account_sid = 'AC01971a0f75f7ed7c8e3f70f94ae38ec7'
         auth_token = '82cb04454043453b889f8ae7b3b51239'
         @client = Twilio::REST::Client.new account_sid, auth_token
          otp = rand.to_s[2..5]
           if User.find_by_otp(otp).present?
              send_otp
  
          else
           self.update_attributes(otp: otp,otp_expiry: Time.now + 2.minutes)
        
          end
          
         @client.api.account.messages.create(
          from: '(706) 666-5786',
         to: "+#{self.mobile}",
          body: 'Hey there! '+otp+' is otp'
           )

      end
      def password_token
          secret_token = Devise.token_generator.generate(User, :fp_token).first

          if User.find_by_fp_token(secret_token).present?
              password_token

          else

              self.update_attributes(fp_token: secret_token,fp_token_expiry: Time.now + 2.hours)
          return nil
          
          end
    end

    def instagram_url
      insta_url = user_social_links.where(:url_type=>"instagram").last
      return "" unless insta_url.present?
      return insta_url.try(:url)
    end

    def facebook_url
      fb_url = user_social_links.where(:url_type=>"facebook").last
      return "" unless fb_url.present?
      return fb_url.try(:url)
    end

    def twitter_url
      twitter_url = user_social_links.where(:url_type=>"twitter").last
        return "" unless twitter_url.present?
      return twitter_url.try(:url)
    end

    def spotify_url 
      spotify_url = user_social_links.where(:url_type=>"spotify").last
      return "" unless spotify_url.present?
      return spotify_url.try(:url)
    end

    def itune_url
      itune_url = user_social_links.where(:url_type=>"itune").last
       return "" unless itune_url.present?
      return itune_url.try(:url)
    end

    def soundcloud_url
     cloud_url = user_social_links.where(:url_type=>"soundcloud").last
      return "" unless cloud_url.present?
      return cloud_url.try(:url)
    end

    # def send_notification(hsh)
    #   self.user.notifications.create(:notification_type => "Points",:message=>"Hooray! you have earned 1 point ")
    # end
end
def employee_mailer(return_hash)
 
    UserMailer.forgot_password_mailer(return_hash).deliver_now
    
    
  end
private
  def process_uri(uri)
    require 'open-uri'
    require 'open_uri_redirections'
    open(uri, :allow_redirections => :safe) do |r|
      r.base_uri.to_s
    end
  end