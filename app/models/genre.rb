class Genre < ApplicationRecord
	has_many :albums,dependent: :destroy
	has_many :songs,through: :albums
	has_many :user_genres,dependent: :destroy
	has_many :users, through: :albums,dependent: :destroy
	mount_uploader :image, GenreUploader
	mount_base64_uploader :image, GenreUploader
	mount_uploader :icon_image, GenreUploader
	mount_base64_uploader :icon_image, GenreUploader

	 def current_uploaded_songs
	 	songs.where(:is_submit => 1)
	 end

	 def remove_uploaded_songs
	 	 current_uploaded_songs.map(&:destroy)
	 	# current_uploaded_songs.update_all(:is_submit => 2)
	 end

	 def songs_available_to_upload
	 	songs.where(:is_submit => 0)
	 end

	 def upload_songs_and_remove_old_songs
	 	remove_uploaded_songs
	 	first_thousand_songs_for_upload.update_all(:is_submit => 1)
	 	remaing_upload if current_uploaded_songs.count < 1000
	 end

	 def max_songs_to_upload
	 	return 1000
	 end

	 def remaing_upload
	 	remaining_song_to_upload = 1000 - current_uploaded_songs.count
	 	remaining_album_ids = albums.map(&:id) - current_uploaded_songs.map(&:album_id)
	 	next_album = Album.where(:id => remaining_album_ids).joins(:songs).order("songs.id asc").first
	 	if next_album.present?
	 	next_album.songs.where(:is_submit => 0).order("id asc").limit(remaining_song_to_upload).update_all(:is_submit => 1)
	    end
	 end
     def self.upload_all_genre_song

      # all.each {|gr| gr.upload_songs_and_remove_old_songs}
      self.delay(queue: "Job", priority: 5, run_at: 7.days.from_now).upload_all_genre_song
     
     end
	 def first_thousand_songs_for_upload
	 	# puts "songs_available_to_upload"
	 	# puts songs_available_to_upload
	 	# puts "max_songs_to_upload"
	 	# puts max_songs_to_upload
	 	if songs_available_to_upload.count > max_songs_to_upload
	 		album_ids = songs.map(&:album_id).uniq
	 		albums = Album.where(:id => album_ids)
		 	# puts "album_ids"
		 	# puts album_ids
	 		upload_albums_array = []
	 		song_count = 0
	 		albums.joins(:songs).order("songs.id asc").uniq.each do |album|
		 	# puts "album"
		 	# puts album.id
	 			if song_count < max_songs_to_upload
		 	# puts "song_count"
		 	# puts song_count
		 	# puts "max_songs_to_upload"
		 	# puts max_songs_to_upload
	 				current_song_count = album.songs.count
	 				if current_song_count < (max_songs_to_upload - song_count)
		 				song_count = song_count + current_song_count
		 				upload_albums_array.push album.id
		 			end
	 			else
	 				break
	 			end
	 		end
		 	puts "upload_albums_array"
		 	puts upload_albums_array
	 			songs = Song.where(:album_id => upload_albums_array) 
		 	puts "songs"
		 	# puts songs
	 			return songs
	 	else
		 	puts "else"
		 	# puts songs_available_to_upload
	 		return songs_available_to_upload
	 	end
	 end
end
