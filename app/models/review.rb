class Review < ApplicationRecord
	acts_as_paranoid
	belongs_to :reviewable, polymorphic: true
	belongs_to :reviewer , class_name: "User"

	def reviewer_detail
		hsh = {}
		return hsh if reviewer.blank?
		hsh[:id] = reviewer.id
		hsh[:first_name] = reviewer.first_name
		hsh[:last_name] = reviewer.last_name
		hsh[:image] = reviewer.image
	
		return hsh
	end
end
