module ApplicationHelper
	def age_lists 
  I18n.t(:age_lists).map { |key, value| [ value, key ] } 
end
def sortable(column, title = nil)
    title ||= column.titleize
    css_class = (column == sort_column) ? "current #{sort_direction}" : nil
    direction = (column == sort_column && sort_direction == "asc") ? "desc" : "asc"
    link_to title, {:sort => column, :direction => direction}, {:class => css_class}
  end
  def show_errors(object, field_name)
  if object.errors.any?
    if object.errors.messages[field_name]
      return object.errors.messages[field_name].join(", ")
    end
  end
end 
def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end
end
