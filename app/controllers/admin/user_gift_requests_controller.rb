class Admin::UserGiftRequestsController < ApplicationController
  before_action :set_user_gift_request, only: [:show, :edit, :update, :destroy]
layout 'admin'
  # GET /admin/user_gift_requests
  # GET /admin/user_gift_requests.json
  def index
    @user_gift_requests = UserGiftRequest.all
  end
  def gift_request_send
    begin
    @gift =  UserGiftRequest.find(params[:id])
    @offer =  Offer.find(@gift.offer_id)
    @user =  User.find(@gift.user_id)
    userpoints  = @user.total_points-@offer.points
    @user.update(:total_points=>userpoints)
    
     @gift.update(:status=>true)
    if  @user.gift_notification==true
      reciever = @user
      hsh = {}
        hsh = HashWithIndifferentAccess.new
        hsh[:title] = "Gift Card"
        hsh[:message] = "Your  points turned into Gift Card now!"
        hsh[:notification_type] = "gift_card"
        hsh[:notification_type_id] = @user.id
        @user.send_notification_by_admin(hsh,reciever)
      end
     flash[:success] = "Gift card sent successfully!"
    redirect_to admin_user_gift_requests_path
   rescue Exception => e
      err_hash = HashWithIndifferentAccess.new
      err_hash[:error] = e.message
      status = :bad_request
      render :json => err_hash.to_json, status: status
   end
  end
  # GET /admin/user_gift_requests/1
  # GET /admin/user_gift_requests/1.json
  def show
  end

  # GET /admin/user_gift_requests/new
  def new
    @user_gift_request = UserGiftRequest.new
  end

  # GET /admin/user_gift_requests/1/edit
  def edit
  end

  # POST /admin/user_gift_requests
  # POST /admin/user_gift_requests.json
  def create
    @user_gift_request = UserGiftRequest.new(user_gift_request_params)

    respond_to do |format|
      if @user_gift_request.save
        format.html { redirect_to admin_user_gift_requests_path, notice: 'User gift request was successfully created.' }
        format.json { render action: 'show', status: :created, location: @user_gift_request }
      else
        format.html { render action: 'new' }
        format.json { render json: @user_gift_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/user_gift_requests/1
  # PATCH/PUT /admin/user_gift_requests/1.json
  def update
    respond_to do |format|
      if @user_gift_request.update(user_gift_request_params)
        format.html { redirect_to admin_user_gift_requests_path, notice: 'User gift request was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @user_gift_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/user_gift_requests/1
  # DELETE /admin/user_gift_requests/1.json
  def destroy
    @user_gift_request.destroy
    respond_to do |format|
      format.html { redirect_to admin_user_gift_requests_url, notice: 'User gift request was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user_gift_request
      @user_gift_request = UserGiftRequest.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_gift_request_params
      params.require(:user_gift_request).permit(:id, :user_id, :offer_id, :created_at, :status)
    end
end
