class Admin::UsersController <  Admin::ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  layout 'admin'
  helper_method :sort_column, :sort_direction
  # GET /admin/users
  # GET /admin/users.json
  before_action :save_my_previous_url

    def save_my_previous_url
    # session[:previous_url] is a Rails built-in variable to save last url.
     session[:my_previous_url] = URI(request.referer || '').path
   end
   def index
    
    if params[:commit] == "Search"
      if params[:artist].present?
      @users = User.where(admin: false).where(role: "A").order("created_at desc").search(params[:user][:text]).paginate(:page => params[:page])
       
       
      else
      @users = User.where(admin: false).where(role: "U").order("created_at desc").search(params[:user][:text]).paginate(:page => params[:page])
    
      end
      
    else
      if params[:direction].present?
         if params[:artist].present?
        @users = User.order(params[:sort] + ' ' + params[:direction]).where(admin: false).where(role: "A").paginate(:page => params[:page])
        else
           @users = User.order(params[:sort] + ' ' + params[:direction]).where(admin: false).where(role: "U").paginate(:page => params[:page])
      end
      else
         if params[:artist].present?
        @users = User.where(admin: false).where(role: "A").order("created_at desc").paginate(:page => params[:page])
        else
           @users = User.where(admin: false).where(role: "U").order("created_at desc").paginate(:page => params[:page])
           end
      end
    end
  end
    def check_email
     
        user =      User.find(params[:user_id])
        check = User.where.not(id:  user.id).find_by_email(params[:user][:email])
        # if(user.email!=params[:user][:email])
        #   @user = User.find_by_email(params[:user][:email])
        #   end
          
          hsh={}
          if check.present?
            hsh=false
          else
            hsh=true
          end
          render :json => hsh
        # if current_user.present?
        #   if(current_user.email!=params[:user][:email])
        #   @user = User.find_by_email(params[:user][:email])
        #   end
        #   else
        #   @user = User.find_by_email(params[:user][:email])
        # end
        

      end
    def sort_direction
      %w[asc desc].include?(params[:direction]) ?  params[:direction] : "asc"
  end

  def sort_column
      User.column_names.include?(params[:sort]) ? params[:sort] : "name"
  end
def dashboard
end
  # GET /admin/users/1
  # GET /admin/users/1.json
  def show
  end

  # GET /admin/users/new
  def new
    @user = User.new
  end

  # GET /admin/users/1/edit
  def edit
  end

  # POST /admin/users
  # POST /admin/users.json
  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to [:admin, @user], notice: 'User was successfully created.' }
        format.json { render action: 'show', status: :created, location: @user }
      else
        format.html { render action: 'new' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end
  def status
    user = User.find(params[:user_id])
    user.update_attributes(status: !(user.status))
    redirect_to  admin_users_path
  end
  # PATCH/PUT /admin/users/1
  # PATCH/PUT /admin/users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to [:admin, @user], notice: 'User was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end
  def change_password
  # debugger
  end
  def update_password
        

  if current_user.valid_password?(user_params[:current_password])     
      current_user.update(:password=>user_params[:password])
    
    else
      flash[:error] = "Please enter correct current password!"
      redirect_to   admin_change_password_path
    end
  end
   def sort_direction
      %w[asc desc].include?(params[:direction]) ?  params[:direction] : "asc"
  end

  def sort_column
      User.column_names.include?(params[:sort]) ? params[:sort] : "name"
  end
  # DELETE /admin/users/1
  # DELETE /admin/users/1.json
  def destroy
    puts "in destroy"
  @user.destroy

    puts "after destroy"
    respond_to do |format|
    puts "in format"
      format.html { redirect_to admin_users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:first_name, :last_name, :image, :email, :city, :state, :mobile, :status,:admin,:role)
    end
end
