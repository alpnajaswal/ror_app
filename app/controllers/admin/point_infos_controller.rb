class Admin::PointInfosController < ApplicationController
  before_action :set_point_info, only: [:show, :edit, :update, :destroy]
  layout 'admin'
  # GET /admin/point_infos
  # GET /admin/point_infos.json
  def index
    @point_infos = PointInfo.all
  end

  # GET /admin/point_infos/1
  # GET /admin/point_infos/1.json
  def show
  end

  # GET /admin/point_infos/new
  def new
    @point_info = PointInfo.new
  end

  # GET /admin/point_infos/1/edit
  def edit
  end

  # POST /admin/point_infos
  # POST /admin/point_infos.json
  def create
    @point_info = PointInfo.new(point_info_params)

    respond_to do |format|
      if @point_info.save
        format.html { redirect_to admin_point_infos_path, notice: 'Point info was successfully created.' }
        format.json { render action: 'show', status: :created, location: @point_info }
      else
        format.html { render action: 'new' }
        format.json { render json: @point_info.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/point_infos/1
  # PATCH/PUT /admin/point_infos/1.json
  def update
    respond_to do |format|
      if @point_info.update(point_info_params)
        format.html { redirect_to admin_point_infos_path, notice: 'Point info was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @point_info.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/point_infos/1
  # DELETE /admin/point_infos/1.json
  def destroy
    @point_info.destroy
    respond_to do |format|
      format.html { redirect_to admin_point_infos_path, notice: 'Point info was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_point_info
      @point_info = PointInfo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def point_info_params
      params.require(:point_info).permit(:title, :description, :status)
    end
end
