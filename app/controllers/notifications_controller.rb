class NotificationsController < ApplicationController
	 before_action :authenticate_user!
	  before_action :check_user_privilige
	def index
	@notification =  current_user.notifications.order("id desc").paginate(:page => params[:page])
	
	end
	 def check_user_privilige
      if current_user
       if current_user.admin
        redirect_to  admin_path
      end
    end
  end
	def unread_notification
  
	notification= current_user.notifications.where(:read=>false).order("id desc")
	if params[:read].present?
	current_user.notifications.update(:read=>true)
	end
	render :json => notification
	end


   def update_notification
   	
    current_user.update(:profile_notification=>params[:profile_notification],:song_notification=>params[:song_notification])
    	flash[:success] = "Notification setting updated"	
         redirect_to  setting_path 
   end
end
