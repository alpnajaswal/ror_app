class AlbumsController < ApplicationController

	 before_action :check_login
   before_action :authenticate_user!
    before_action :check_user_privilige
  def index
    

		@albums = current_user.albums
	end
	def add_album
		
	end
   def check_user_privilige
   
   
   
    if current_user
      if current_user.admin
        redirect_to  admin_path
      end
    end
  end
  def republic
  
    
   @album =  Album.find_by_id(params[:id])
   @songs = Song.where(:album_id=>params[:id]).order("id asc")
  
  end
	 def create
  tdate =  Date.today
  date = tdate.strftime("%Y-%m-%d")
  @lbum =Album.where("release_date <= ?", date).order("id desc").last      
  # album_params[:release_date] =  @lbum.release_date+14.days
   # params[:album][:release_date] = @lbum.release_date+14.days
   params[:album][:release_date] = tdate+1.day
    
    @album = Album.new(album_params)
 
    # respond_to do |format|
      if @album.save
        flash[:error] = "success"
         redirect_to albums_path
       
      else
        format.html { render :new }
        format.json { render json: @album.errors, status: :unprocessable_entity }
      end
    # end
  end

  # PATCH/PUT /addresses/1
  # PATCH/PUT /addresses/1.json
  def update
    # respond_to do |format|
      if @album.update(address_params)
        flash[:error] = "success"
        redirect_to albums_path
        # format.html { redirect_to @address, notice: 'Address was successfully updated.' }
        # format.json { render :show, status: :ok, location: @address }
      else
        format.html { render :edit }
        format.json { render json: @address.errors, status: :unprocessable_entity }
      end
    # end
  end
   private
    # Use callbacks to share common setup or constraints between actions.
   

    # Never trust parameters from the scary internet, only allow the white list through.
    def album_params
      params.require(:album).permit(:id, :title, :user_id, :image, :description,:genre_id,:release_date)
    end
end
