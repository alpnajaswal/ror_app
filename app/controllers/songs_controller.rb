class SongsController < ApplicationController
	 
   before_action :check_login
    before_action :authenticate_user!
    before_action :check_user_privilige
	 def create
	      @new_song = Song.where(:album_id=>song_params[:album_id]).where(:is_submit=>0).count
         if @new_song==100
          flash[:error] = "You have already 100 songs in this album to release please try next week"
         redirect_to "/albums/"+song_params[:album_id]+"/album_songs"
         else
         
      params[:song][:is_submit]=1

     
       @song = Song.new(song_params)
      if @song.save
        flash[:error] = "success"
         redirect_to "/albums/"+song_params[:album_id]+"/album_songs"
      else
        format.html { render :new }
        format.json { render json: @song.errors, status: :unprocessable_entity }
      end 
    end
   end
   def check_user_privilige
   
   
   
    if current_user
      if current_user.admin
        redirect_to  admin_path
      end
    end
  end
    def update
      @song = Song.find(params[:id])
    if @song.update(song_params)
        
        redirect_to "/albums/#{@song.album_id}/album_songs"
     else
        format.html { render :edit }
        format.json { render json: @address.errors, status: :unprocessable_entity }
      end
  end

   private
    # Use callbacks to share common setup or constraints between actions.
   

    # Never trust parameters from the scary internet, only allow the white list through.
    def song_params
      params.require(:song).permit(:id, :title, :album_id, :file, :description,:duration,:is_submit)
    end
end
