class UsersController < ApplicationController
	  before_action :authenticate_user!
	 before_action :check_login
	 before_action :save_my_previous_url
	 before_action :check_user_privilige

    def save_my_previous_url
    # session[:previous_url] is a Rails built-in variable to save last url.
     session[:my_previous_url] = URI(request.referer || '').path
   end

	 def my_profile
	 	

	 	@profile_like =   current_user.favourites.count
	 	@song_like =   Favourite.where(:favourable_id=>current_user.songs,:favourable_type=>'Song').count
	    @song_view =   RecentlyPlay.where(:song_id=>current_user.songs).count
	    @facebook = current_user.user_social_links.find_by(url_type: "facebook")
	  @instagram = current_user.user_social_links.find_by(url_type: "instagram")
	  @twitter = current_user.user_social_links.find_by(url_type: "twitter")
	 end
	 def profile
	 	@genere = Genre.all
	 	return_hash = HashWithIndifferentAccess.new
	 	return_hash[:message]= current_user.first_name+" "+current_user.last_name+" new artist has been register on tune reward stay tune to listen new  songs"
	    @alluser = SubscribeNews.all
	    @alluser.each do |user|
	    
           return_hash[:email]= user.email
           # UserMailer.news_letter_mailer(return_hash).deliver_now;
           UserMailer.delay(run_at: 5.seconds.from_now).news_letter_mailer(return_hash)
         
      end
	 end
	  def usersetting
		 
	  @facebook = current_user.user_social_links.find_by(url_type: "facebook")
	  @instagram = current_user.user_social_links.find_by(url_type: "instagram")
	  @twitter = current_user.user_social_links.find_by(url_type: "twitter")
	   @itune = current_user.user_social_links.find_by(url_type: "itune")
	  @spotify = current_user.user_social_links.find_by(url_type: "spotify")
	  @soundcloud = current_user.user_social_links.find_by(url_type: "soundcloud")
	 end
	 
   def update_profile
   	 params[:user][:dob] = params[:user][:dob].to_date
     params[:user][:dob] = params[:user][:dob].strftime('%Y-%m-%d')
     current_user.update(:age=>params[:user][:age],:dob=>params[:user][:dob],:first_name=>params[:user][:first_name],:last_name=>params[:user][:last_name],:image=>params[:user][:image],:username=>params[:user][:username])
	 current_user.build_user_genre(genre_id:params[:genre_id]).save
      if params[:url].present?
         params[:url].each do |key, value|
     	if current_user.user_social_links.find_by(url_type: key).present?
     	current_user.user_social_links.where(url_type:key).update(url:value)
     	else
	   current_user.user_social_links.create(url_type:key,url:value)
	  end	
	end
 end
	redirect_to my_profile_path
	end
   def update_password
	
	if current_user.valid_password?(params[:user][:current_password])			
      current_user.update(:password=>params[:user][:password])
	  redirect_to  new_user_session_path
    else
    	flash[:danger] = "Current password is not correct.Please enter correct current password!"
    	redirect_to    usersetting_path
    end
	end
	 def check_user_privilige
      if current_user
       if current_user.admin
        redirect_to  admin_path
      end
    end
  end
end
