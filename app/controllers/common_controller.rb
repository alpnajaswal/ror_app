class CommonController < ApplicationController
  skip_before_action :verify_authenticity_token
  
    before_action :check_user_privilige
	def index
    # @users = User.where(:role=>"A").where(:id => Favourite.where(:favourable_type => "User").group(:favourable_id).order('count_id DESC').count(:id).keys )
    # @users = User.where(:id => Favourite.where(:favourable_type => "User").group(:user_id).order('count_id DESC').count(:id).keys ).limit(10)
    # @users = User.where(:id => Favourite.where(:favourable_type => "User").group(:user_id).order('count_id DESC').count(:id).keys ).limit(10)
    @users = Favourite.popular
	end
      
   def check_email
       	# if(current_user.email!=params[:user][:email])
        #   @user = User.find_by_email(params[:user][:email])
         
        #  	else
          @user = User.find_by_email(params[:user][:email])
        # end
      hsh={}
    if  @user.present?
            hsh=false
          else
            hsh=true
          end
          render :json => hsh
    # respond_to do |format|
    # format.json { render :json => !@user }
    #  end
    end
     def check_user_privilige
   
   
   
    if current_user
      if current_user.admin
        redirect_to  admin_path
      end
    end
  end
    def social_login

      begin
        
      login_with_facebook(params) 
      @user.update_attributes!(role: "A")
      current_user = @user
      # raise "This account has been suspended by admin" if @user.suspend
       sign_in(@user, scope: :user)
        if(@user.age==nil)
        render :js => "window.location.href = '/profile'"
      else
        render :js => "window.location.href = '/my_profile'"
      end
         # redirect_to profile_path
      rescue Exception => e
        puts e.message
        err_hash={}
        err_hash[:error]=e.message
        render :json => err_hash.to_json, status: :bad_request 
      end
      
    end
        def login_with_facebook(params)
        
      fb_info = User.connect_with_facebook(params[:access_token])
    
      raise "Access token is invalid." if fb_info[:error].present?
        email = fb_info[:email].nil? ? "#{fb_info[:id]}@facebook.com" : fb_info[:email]
        if User.find_by(email: fb_info[:email]).present?
          @user = User.find_by(email: fb_info[:email])
          # raise "Your email is not verified,please verify before login." unless @user.is_email_verified.try(:to_bool)
          # raise "Please wait till your account is verified by admin." if @user.approve.blank? && @user.profile_completed.present?
        elsif User.find_by(social_id: fb_info[:id]).present?
          @user = User.find_by(social_id: fb_info[:id])
          # raise "Your email is not verified,please verify before login." unless @user.is_email_verified.try(:to_bool)
          # raise "Please wait till your account is verified by admin." if @user.approve.blank? && @user.profile_completed.present?
        elsif fb_info[:error].blank?
          fb_id = fb_info[:id]
          first_name = fb_info[:first_name].nil? ? "" : fb_info[:first_name]
          last_name = fb_info[:last_name].nil? ? "" : fb_info[:last_name]
          full_name = first_name + " " + last_name
          password = Devise.friendly_token
          verified_status =  fb_info[:email].present? ? true : false
          @user = User.new(:first_name=>first_name,:last_name=>last_name,:social_id=>fb_id,:email=>email,:password=>password,role: "A")
          image_url = "https://graph.facebook.com/#{fb_info[:id]}/picture?type=large"
          avatar_url =image_url.gsub("­http","htt­ps")
          avatar_url_new = process_uri(avatar_url)
          @user.image = avatar_url
          @user.save

        else
        raise "access token is invalid"
      end

      # @user.update_attributes!(:current_time_zone => params[:user][:current_time_zone]) if params[:user][:current_time_zone].present?   
    end
    def set_password
       @user = User.find_by_fp_token(params[:token])
       
    end
def update_password
  @user = User.find_by_fp_token(params[:user][:fp_token])
   if @user.present?  
      
      @user.update(:password=>params[:user][:confirm_password],:fp_token=>"")
      flash[:success] = "Password Changed successfully!"
      redirect_to  root_path
    else
      flash[:danger] = "Forget password link expired!"
      redirect_to    root_path
    end
  end

end
