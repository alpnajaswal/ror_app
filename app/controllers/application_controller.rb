class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :set_mail_host


def set_mail_host 
  ActionMailer::Base.default_url_options = {:host => request.host_with_port} 
end
 def after_sign_in_path_for(resource)
  
      begin
        if resource.admin
           admin_path 
        else
         #abc = stored_location_for(resource)
            if(resource.role=='A')
              
            	if(resource.dob==nil)
        	        profile_path
                  else
                  	 my_profile_path
                  end
             else
          	   destroy_user_session_path
          
             end
        end
      rescue Exception => e
        
      end

    end
   
   def configure_permitted_parameters
     
    devise_parameter_sanitizer.permit(:sign_up, keys: [:first_name, :last_name, :image, :email, :city, :state, :mobile, :status,:role,:username])
  end
  private
    # Use callbacks to share common setup or constraints between actions.
    def check_login
     unless current_user.present?
    redirect_to root_path
     end
    end
end
