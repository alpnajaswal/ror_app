class SubscribeNewsController < ApplicationController
	def subscribe_me
		
		@subscribe = SubscribeNews.create(subscribe_params)

		if @subscribe.present?
			return_hash = HashWithIndifferentAccess.new
	 	return_hash[:message]= "Thanks for subscribing email"
	     return_hash[:email]= @subscribe.email
        UserMailer.subscribe_mailer(return_hash).deliver_now;
			flash[:success] = "Your email has been subscribed for newsletter"
		     redirect_to root_path
		 else
		 end
	end

	private
    # Use callbacks to share common setup or constraints between actions.
   

    # Never trust parameters from the scary internet, only allow the white list through.
    def subscribe_params
      params.require(:subscribe_news).permit(:email)
    end
end
