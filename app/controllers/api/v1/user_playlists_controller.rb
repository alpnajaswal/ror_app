class Api::V1::UserPlaylistsController < Api::V1::ApplicationController
	before_action :authenticate_api_user
	before_action :set_playlist, only: [:playlist_songs,:add_song]
	def index
		begin

		rescue Exception => e
			puts "error_exception #{Time.now} #{e.message}"
			err_hash={}
			err_hash[:error]=e.message
			render :json => err_hash.to_json , status: :bad_request 
		end
	end

	def create
		
		begin
			checklist =  @current_api_user.user_playlists.find_by_name(params[:user_playlist][:name])
            if checklist.present?
            	raise "Playlist name already exist"
            else
            @current_api_user.user_playlists.create!(name: params[:user_playlist][:name])
            render :index
		    end
		rescue Exception => e
			puts "error_exception #{Time.now} #{e.message}"
			err_hash={}
			err_hash[:error]=e.message
			render :json => err_hash.to_json , status: :bad_request 
		end
	end
    def delete
    	begin
            @current_api_user.user_playlists.where(:id=>params[:user_playlist_id]).destroy_all
                hsh={}
				hsh[:status]="true"
				hsh[:message]="your playlist has been deleted successfully "	
			render :json => hsh.to_json
		rescue Exception => e
			puts "error_exception #{Time.now} #{e.message}"
			err_hash={}
			err_hash[:error]=e.message
			render :json => err_hash.to_json , status: :bad_request 
		end 
    end
	def playlist_songs
		begin
			@playlist_songs = @playlist.playlist_songs
		rescue Exception => e
			puts "error_exception #{Time.now} #{e.message}"
			err_hash={}
			err_hash[:error]=e.message
			render :json => err_hash.to_json , status: :bad_request 
		end
	end

	def add_song
		begin
			@playlist.playlist_songs.create!(song_id: params[:user_playlist][:song_id])
			render :playlist_songs
		rescue Exception => e
			puts "error_exception #{Time.now} #{e.message}"
			err_hash={}
			err_hash[:error]=e.message
			render :json => err_hash.to_json , status: :bad_request 
		end
		
	end

	private

	def set_playlist
		@playlist = @current_api_user.user_playlists.find(params[:user_playlist_id])
	end

end