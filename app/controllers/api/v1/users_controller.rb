class Api::V1::UsersController < Api::V1::ApplicationController
before_action :authenticate_api_user , except: [:sign_up,:add_new_point,:sign_in,:sign_up_social,:reset_password,:forgot_password,:varify_forgot_otp,:update_password,:resend_otp,:social_login,:varify_otp]
before_action :set_user, only: [:follow,:unfollow,:add_to_favourite,:remove_from_favorite]
		def sign_up
			begin
				
				user = User.find_by(mobile: signup_params[:mobile])
				 # if user.present?
			  #    raise "Mobile number already exist"
     #               else
					@user=User.create!(signup_params)
					@user.send_otp
					@user.create_token
					@current_api_user=@user

					render :me
				   # end
		 	rescue Exception => e
		 		puts "error_exception #{Time.now} #{e.message}"
				err_hash={}
				err_hash[:error]=e.message
				render :json => err_hash.to_json  , status: :bad_request 
		 	end
		end

		def sign_in
		  begin
			email_id = params[:user][:email]
		    mobile = params[:user][:mobile]
		if email_id.present?
			user = User.find_by(email: params[:user][:email])
			raise "User Not Exist" if user.nil?
			else
			user = User.find_by(mobile: params[:user][:mobile])
			raise "User Not Exist" if user.nil?
				    end
		if user.valid_password?(params[:user][:password])
					user.create_token 
					user.update_attributes(:device_type=> params.dig(:user,:device_type),:device_id=> params.dig(:user,:device_id))
					@user=user
					@current_api_user=user
					render :me
				else 
					raise "Password Not valid"	
				end
		  rescue Exception => e
			  	puts e.message
				err_hash={}
				err_hash[:error]=e.message
				render :json => err_hash.to_json, status: :bad_request 
		  end  	
		end
		def social_login

			begin
				
			login_with_facebook(params) 
			@user.update_attributes!(role: "U",device_id: params[:user][:device_id],device_type: params[:user][:device_type])
			@current_api_user = @user
			# raise "This account has been suspended by admin" if @user.suspend
			@user.create_token

			render :me
			rescue Exception => e
				puts e.message
				err_hash={}
				err_hash[:error]=e.message
				render :json => err_hash.to_json, status: :bad_request 
			end
			
		end
        def login_with_facebook(params)
				
			fb_info = User.connect_with_facebook(params[:user][:access_token])
		
			raise "Access token is invalid." if fb_info[:error].present?
		    email = fb_info[:email].nil? ? "#{fb_info[:id]}@facebook.com" : fb_info[:email]
		    if User.find_by(email: fb_info[:email]).present?
		    	@user = User.find_by(email: fb_info[:email])
		    	# raise "Your email is not verified,please verify before login." unless @user.is_email_verified.try(:to_bool)
		   		# raise "Please wait till your account is verified by admin." if @user.approve.blank? && @user.profile_completed.present?
		    elsif User.find_by(social_id: fb_info[:id]).present?
		    	@user = User.find_by(social_id: fb_info[:id])
		    	# raise "Your email is not verified,please verify before login." unless @user.is_email_verified.try(:to_bool)
		    	# raise "Please wait till your account is verified by admin." if @user.approve.blank? && @user.profile_completed.present?
		    elsif fb_info[:error].blank?
			    fb_id = fb_info[:id]
			    first_name = fb_info[:first_name].nil? ? "" : fb_info[:first_name]
			    last_name = fb_info[:last_name].nil? ? "" : fb_info[:last_name]
			    full_name = first_name + " " + last_name
			    password = Devise.friendly_token
		    	verified_status =  fb_info[:email].present? ? true : false
			    @user = User.new(:first_name=>first_name,:last_name=>last_name,:social_id=>fb_id,:email=>email,:password=>password,role: "U")
				image_url = "https://graph.facebook.com/#{fb_info[:id]}/picture?type=large"
				avatar_url =image_url.gsub("­http","htt­ps")
				avatar_url_new = process_uri(avatar_url)
				@user.image = avatar_url
				@user.save

		    else
				raise "access token is invalid"
			end

			# @user.update_attributes!(:current_time_zone => params[:user][:current_time_zone]) if params[:user][:current_time_zone].present?	 	
		end
		def me
			begin
			
				render :me	
			rescue Exception => e
				err_hash={}
				err_hash[:error]=e.message
				render :json => err_hash.to_json, status: :bad_request 
			end
		
		end
         def update
			
			begin
		    @current_api_user.update(signup_params)
			render :me
				
		 	rescue Exception => e
		 		puts "error_exception #{Time.now} #{e.message}"
				err_hash={}
				err_hash[:error]=e.message
				render :json => err_hash.to_json  , status: :bad_request 
		 	end
		end
		 
		def sign_out
			begin
				@current_api_user.update_attributes(:api_secret=>"",:device_id => "",:device_type => "")
				hsh={}
				hsh[:status]="true"
				hsh[:message]="sucessfully updated"						
			rescue Exception => e
		 		puts "error_exception #{Time.now} #{e.message}"
				err_hash={}
				err_hash[:error]=e.message
				render :json => err_hash.to_json , status: :bad_request 			
			end
		end
		def recent_music
			begin
				@current_api_user.recent_music
				hsh={}
				hsh[:status]="true"
				hsh[:message]="sucessfully updated"						
			rescue Exception => e
		 		puts "error_exception #{Time.now} #{e.message}"
				err_hash={}
				err_hash[:error]=e.message
				render :json => err_hash.to_json , status: :bad_request 			
			end
		end

		def follow
			begin
				@current_api_user.follow(@other_user)
				hsh = {}
				hsh[:message] = "Sucessfully added into following list."
				render :json=>hsh
			rescue Exception => e
				puts "error_exception #{Time.now} #{e.message}"
				err_hash={}
				err_hash[:error]=e.message
				render :json => err_hash.to_json , status: :bad_request 
			end
		end

		def unfollow
			begin
				@current_api_user.stop_following(@other_user)
				hsh = {}
				hsh[:message] = "Sucessfully remove from following list."
				render :json=>hsh
			rescue Exception => e
				puts "error_exception #{Time.now} #{e.message}"
				err_hash={}
				err_hash[:error]=e.message
				render :json => err_hash.to_json , status: :bad_request 
			end
		end
		def followings
			begin
				hsh = {}
				@following = @current_api_user.following_users.select(:id,:first_name,:last_name,:image,:total_points).order("id desc")
				render :following
			rescue Exception => e
				puts "error_exception #{Time.now} #{e.message}"
				err_hash={}
				err_hash[:error]=e.message
				render :json => err_hash.to_json , status: :bad_request 
			end
		end
      def select_genre    	
		begin
			
			 hsh = {}
			  @current_api_user.build_user_genre(genre_id:params[:user_genre][:genre_id]).save
			   hsh[:message] = "Genre selected."
		       render :json=>hsh
		rescue Exception => e
			puts "error_exception #{Time.now} #{e.message}"
			err_hash={}
			err_hash[:error]=e.message
			render :json => err_hash.to_json , status: :bad_request 
		end
	end
      def common_genre    	
		begin
			 hsh = {}
			# @users = @current_api_user.common_genre
			@users =  Favourite.popular
            render :common_genre
		 rescue Exception => e
			puts "error_exception #{Time.now} #{e.message}"
			err_hash={}
			err_hash[:error]=e.message
			render :json => err_hash.to_json , status: :bad_request 
		end
	end

        def profile    	
		begin
			 hsh = {}
			@user = User.find(params[:id])
            render :profile
		 rescue Exception => e
			puts "error_exception #{Time.now} #{e.message}"
			err_hash={}
			err_hash[:error]=e.message
			render :json => err_hash.to_json , status: :bad_request 
		end
	end


		def followers
			begin
				hsh = {}
				@followers = @current_api_user.user_followers.select(:id,:first_name,:last_name,:image,:total_points)
				render :follower
			rescue Exception => e
				puts "error_exception #{Time.now} #{e.message}"
				err_hash={}
				err_hash[:error]=e.message
				render :json => err_hash.to_json , status: :bad_request 
			end
		end
     def add_new_point
         @user = User.find(params[:id])
          @user.add_new_point
           hsh = HashWithIndifferentAccess.new
        hsh[:title] = "Point Earned"
        hsh[:message] = "Hooray you have earned 1 point."
         
      render :json => hsh, status: :Successfully 
      end
      
		def review
			begin
				set_user
                # @reviewable.reviews.create!(reviewer_id: @current_api_user.id,comment: params[:user][:comment])
               @current_api_user.reviews.create!(reviewer_id: @reviewable.id,comment: params[:user][:comment])
				 @current_api_user.check_review_add_point
				render :me
			rescue Exception => e
				puts "error_exception #{Time.now} #{e.message}"
				err_hash={}
				err_hash[:error]=e.message
				render :json => err_hash.to_json , status: :bad_request 
			end
		end

		def reviews
			begin
			rescue Exception => e
				puts "error_exception #{Time.now} #{e.message}"
				err_hash={}
				err_hash[:error]=e.message
				render :json => err_hash.to_json , status: :bad_request 
			end
		end

		def add_to_favourite
		begin
	    is_fav =  @current_api_user.favourites.find_by_user_id(@other_user.id)
		raise "This User is already added to favourites." if is_fav.present?
			# @other_user.favourites.create(user_id: @current_api_user.id)
			 @current_api_user.favourites.create(user_id: @other_user.id)
			# @current_api_user.build_favourites(user_id: @other_user.id).save

		   message = message = @current_api_user.first_name+" "+ @current_api_user.last_name+ " liked your profile"
			if(User.find(@other_user.id).profile_notification==false)
				Notification.create(:notification_type=>"User",:notification_type_id=>@other_user.id,:title=>"Profile Like",:message=>message,:sender_id=>@current_api_user.id,:user_id=>@other_user.id,:read=>true)
			else
			Notification.create(:notification_type=>"User",:notification_type_id=>@other_user.id,:title=>"Profile Like",:message=>message,:sender_id=>@current_api_user.id,:user_id=>@other_user.id)
		     end
			hsh = {}
			hsh[:message] = "Successfully added into favourite list"
			render json: hsh
		rescue Exception => e
			puts "error_exception #{Time.now} #{e.message}"
			err_hash={}
			err_hash[:error]=e.message
			render :json => err_hash.to_json  , status: :bad_request 
		end
	end
def remove_from_favorite
     begin
     	is_fav =  @current_api_user.favourites.find_by_user_id(@other_user.id)
          raise "This User is already removed from favourites." unless is_fav.present?
           	is_fav.destroy
           # @other_user.favourites.find_by(user_id: @current_api_user.id).destroy
           # @current_api_user.favourites.find_by(user_id: @other_user.id).destroy
           	hsh = {}
			hsh[:message] = "Successfully remove from favourite list"
			render json: hsh
		rescue Exception => e
			puts "error_exception #{Time.now} #{e.message}"
			err_hash={}
			err_hash[:error]=e.message
			render :json => err_hash.to_json  , status: :bad_request	
		end

	end	
    def varify_otp
    	begin
    		@user = User.find_by_otp(params[:user][:otp])

		     if @user.present?
		     	if(Time.now > @user.otp_expiry)
		     		hsh = {}
				hsh[:message] = "OTP Expired !!"
				render json: hsh 
		     	else
		     	 hsh = {}
				hsh[:message] = "Varify OTP "
				render json: hsh 
			  end
		     else
		     	hsh = {}
				hsh[:message] = "Invalid OTP "
				render json: hsh
		     end	
	    rescue Exception => e
			puts "error_exception #{Time.now} #{e.message}"
			err_hash={}
			err_hash[:error]=e.message
			render :json => err_hash.to_json  , status: :bad_request	
		end
    end	
    def varify_forgot_otp
    	begin
    		  @user = User.find_by_mobile(params[:user][:mobile])
		     if @user.otp==params[:user][:otp]
		     	if(Time.now>@user.otp_expiry)
		     		hsh = {}
				hsh[:message] = "OTP Expired !!"
				render json: hsh 
		     	else

		     	render :reset_password
			end
		     else
		     	hsh = {}
				hsh[:message] = "Invalid OTP "
				render json: hsh
		     end	
	    rescue Exception => e
			puts "error_exception #{Time.now} #{e.message}"
			err_hash={}
			err_hash[:error]=e.message
			render :json => err_hash.to_json  , status: :bad_request	
		end
    end	
    def resend_otp
    begin
    usr_hsh = HashWithIndifferentAccess.new
    user = User.find_by_mobile(params[:user][:mobile])
    raise "Number not registered." unless user.present?
    user.send_otp
      usr_hsh[:message] ='The otp has been sent to the mobile number '+params[:user][:mobile]+'.'
    render :json => usr_hsh.to_json
    rescue Exception => e
	    err_hash = HashWithIndifferentAccess.new
	    err_hash[:error] = e.message
	    status = :bad_request
	    render :json => err_hash.to_json, status: status
   end
    end
   def forgot_password
   begin
    usr_hsh = HashWithIndifferentAccess.new
    if(params[:user][:email]=='')
    user = User.find_by_mobile(params[:user][:mobile])
   raise "Number not registered." unless user.present?
    user.send_otp
      usr_hsh[:message] ='The otp has been sent to the mobile number '+params[:user][:mobile]+'.'
    render :json => usr_hsh.to_json
   else
    user = User.find_by_email(params[:user][:email].downcase)
    raise "Email not registered." unless user.present?
    user.password_token
    return_hash = HashWithIndifferentAccess.new
    return_hash[:token] = user.fp_token
    return_hash[:base_url] = request.base_url
   return_hash[:user_email] = user.email
    return_hash[:user_name] = user.full_name
    UserMailer.forgot_password_mailer(return_hash).deliver_now
    usr_hsh[:message] ='The password link has been sent to the email address.'
    render :json => usr_hsh.to_json
    end
   rescue Exception => e
    err_hash = HashWithIndifferentAccess.new
    err_hash[:error] = e.message
    status = :bad_request
    render :json => err_hash.to_json, status: status
   end
  end 
    def update_password
     user = User.find_by_id(params[:user][:user_id])
	 user.update(:password=>params[:user][:password])
	 hsh = {}
	 hsh[:message] = "Password Updated Successfully !!"
	 render json: hsh 
	 end
	 def change_current_password
	
	if @current_api_user.valid_password?(params[:user][:current_password])			
      @current_api_user.update(:password=>params[:user][:password])
	   hsh = {}
	 hsh[:message] = "Password Updated Successfully !!"
	 render json: hsh 
    else
    	 hsh = {}
	 hsh[:message] = "Current password is not correct.Please enter correct current password!"
	 render json: hsh 
    
    end
	end
	
   def get_points

   	begin
      @points = @current_api_user.points.order("id desc").limit(3)
   	  hsh = {}
	  hsh[:points] = @current_api_user.total_points
	  hsh[:recent_points] = @points
	 render json: hsh 
	 rescue Exception => e
	    err_hash = HashWithIndifferentAccess.new
	    err_hash[:error] = e.message
	    status = :bad_request
	    render :json => err_hash.to_json, status: status
   end
   end
    def get_offers
         begin
        offer_ids = @current_api_user.user_gift_requests.where(:status=>false).map(&:offer_id).uniq
      
        @offerpoints =  Offer.where(id: offer_ids)

        points =  @offerpoints.sum(&:points)

        offer_prizes =  @offerpoints.sum(&:offer_prize).to_i
      
        # if offer_prizes.include?('$')
        # o = offer_prizes.split("$")
        # offer_pr = o[1].to_i
        #  else
        #  	offer_pr = offer_prizes.to_i
        #   end

        if points==0
        	if @current_api_user.total_points.present?
        	available_points      = @current_api_user.total_points
             else
        	available_points      = 0;
            end
        	puts available_points
        	
        else
       available_points = @current_api_user.total_points - points
       end

        # @offers = Offer.where("points < ?",  available_points)
        @offers = Offer.all
        hsh = {}
   	   if @offers.present?
   	   	if offer_prizes >0 
   	   	hsh[:message] = "Your gift request for $ #{offer_prizes} under processing"
   	    end
	    hsh[:offers] = @offers
	    hsh[:available_points] = available_points
   	   	
		else
		if @offers.count==0
          hsh[:message] = "No offers available for you."
          hsh[:offers] = []
          hsh[:available_points] = available_points
          end
	  end
	 render json: hsh 
	 rescue Exception => e
	    err_hash = HashWithIndifferentAccess.new
	    err_hash[:error] = e.message
	    status = :bad_request
	    render :json => err_hash.to_json, status: status
   end
   end
   def browse
   	begin
       	text = params[:user][:search]
       	hsh={}
		@album = Album.joins(:songs).where('songs.is_submit'=>1).where('albums.title ILIKE ?', "%#{text}%")
		@song = Song.where('is_submit'=>1).where('title ILIKE ?', "%#{text}%")
		@genre = Genre.where('name ILIKE ?', "%#{text}%")
		@artist = User.where('first_name ILIKE ? OR last_name ILIKE ? OR username ILIKE ?', "%#{text}%", "%#{text}%","%#{text}%").where(:role=>"A")
		
		# unless @album.present? && @song.present? && @genre.present? && @artist.present?
			
		# 	hsh[:message]  = "No data found "
		
			
		# end
        render :brows
		# hsh[:album] = Album.where("title like ?"," %#{text}%")
		# hsh[:song] = Song.where("title like","%#{text}%")
		# hsh[:genre] = Genre.where("name like","%#{text}%")
		# hsh[:artist] =User.where("name like").where(:role=>"A")
       
       rescue Exception => e
			puts "error_exception #{Time.now} #{e.message}"
			err_hash={}
			err_hash[:error]=e.message
			render :json => err_hash.to_json  , status: :bad_request	
		end
      end
      def setting_notification

   	     
        @current_api_user.update(signup_params)
    	 
    	 hsh = {}
    	 hsh[:message] = "Notification setting updated Successfully."
        render json: hsh 
   end
     def all_notification
   	
        
    	 hsh = {}
    	 
    	 	@current_api_user.notifications.where(:read=>false).update(:read=>true)
    	  hsh[:notification_count] = @current_api_user.notifications.where(:read=>false).count
    	 hsh[:notifications] = @current_api_user.notifications.order("id desc")
        
        render json: hsh 
   end
   def unread_notification
   	  
   	    hsh = {}
   	    hsh[:notification_count] = @current_api_user.notifications.where(:read=>false).count
        render json: hsh 
   end
	private

		def set_user
			 @other_user = User.find(params[:user_id]) if params[:user_id].present?
			@reviewable = User.find(params[:user][:reviewable_id]) if params.dig(:user,:reviewable_id).present?
		end
		def signup_params
			params.require(:user).permit(:first_name, :last_name, :image, :email,:password, :city, :state, :mobile, :status,:admin,:role,:device_type,:device_id,:point_notification,:gift_notification)
		end
end