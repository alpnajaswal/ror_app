class Api::V1::SongsController < Api::V1::ApplicationController
	before_action :set_song,only: [:add_to_favourite,:remove_from_favorite,:review,:reviews]
	def play
		begin
			@song = Song.find_by_id(params[:id])
		
          
			@new_view = @song.view_count+1
			@song.update(:view_count=>@new_view)
			# recentsong = @current_api_user.recently_plays.find_or_create_by(:song_id=>params[:id])
			recentsong = @current_api_user.recently_plays.create(:song_id=>params[:id])
			
			recentsong.update(:updated_at=>Time.now)
			hhs={}
			hhs[:message]="Added in recent playlist successfully"
			render :json => hhs.to_json  
		rescue Exception => e
			puts "error_exception #{Time.now} #{e.message}"
			err_hash={}
			err_hash[:error]=e.message
			render :json => err_hash.to_json  , status: :bad_request 
		end
	end


	def add_to_favourite
		begin
		is_fav = @song.favourites.find_by_user_id(@current_api_user.id)
		raise "This song is already added to favourites." if is_fav.present?
		@song.favourites.create(user_id: @current_api_user.id)
		  message = message = @song.title + " song liked by "+ @current_api_user.first_name + " " + @current_api_user.last_name 
		@album = Album.find(@song.album_id)

	  if(User.find(@album.user_id).profile_notification.present? && User.find(@album.user_id).profile_notification==false)

			Notification.create(:notification_type=>"Song",:notification_type_id=>@song.id,:title=>"Song Like",:message=>message,:sender_id=>@current_api_user.id,:user_id=>@album.user_id,:read=>true)
		else
         Notification.create(:notification_type=>"Song",:notification_type_id=>@song.id,:title=>"Song Like",:message=>message,:sender_id=>@current_api_user.id,:user_id=>@album.user_id)
		end
			hsh = {}
			hsh[:message] = "Successfully added into favourite list"
			render json: hsh
		rescue Exception => e
			puts "error_exception #{Time.now} #{e.message}"
			err_hash={}
			err_hash[:error]=e.message
			render :json => err_hash.to_json  , status: :bad_request 
		end
	end

	def remove_from_favorite

		begin
			is_fav = @song.favourites.find_by_user_id(@current_api_user.id)
			raise "This song is already removed from favourites." unless is_fav.present?
           	is_fav.destroy
           	hsh = {}
			hsh[:message] = "Successfully remove from favourite list"
			render json: hsh
		rescue Exception => e
			puts "error_exception #{Time.now} #{e.message}"
			err_hash={}
			err_hash[:error]=e.message
			render :json => err_hash.to_json  , status: :bad_request	
		end

	end
  def full_play
		begin
       @full_song = @current_api_user.recently_plays.where(:song_id=>params[:id]).last
        @full_song.update(:full_listen=> true)
		      	
			@full_song.check_and_add_points
           
			hhs={}
			hhs[:message]="Added in recent playlist successfully"
			hhs[:updated_count]= @current_api_user.notifications.where(:read=>false).count
			render :json => hhs.to_json  
		rescue Exception => e
			puts "error_exception #{Time.now} #{e.message}"
			err_hash={}
			err_hash[:error]=e.message
			render :json => err_hash.to_json  , status: :bad_request 
		end
	end
	def recent_play_list

		begin
		
			# @recentsong = @current_api_user.recently_plays.group(&:song_id).order("id desc").limit(20)
			      @recentsong_tmp = RecentlyPlay.where(:user_id => @current_api_user).order('id desc').uniq { |p| p.song_id } 
			      arr = @recentsong_tmp.first(20)
			      @recentsong = RecentlyPlay.where(:id => arr).limit(20)

			# @recentsong = RecentlyPlay.where(:user_id=>@current_api_user).order("id asc")
		# @recentsong = @current_api_user.recently_plays.joins(:song).group('id').limit(20)
			render :recent_song
		rescue Exception => e
			puts "error_exception #{Time.now} #{e.message}"
			err_hash={}
			err_hash[:error]=e.message
			render :json => err_hash.to_json  , status: :bad_request 
		end
	end
    def all_songs
		begin
			@recentsong = Song.all.where(:is_submit=>1)
			render :song
		rescue Exception => e
			puts "error_exception #{Time.now} #{e.message}"
			err_hash={}
			err_hash[:error]=e.message
			render :json => err_hash.to_json  , status: :bad_request 
		end
	end
	def review
			begin
				@song.reviews.create!(reviewer_id: @current_api_user.id,comment: params[:song][:comment])
				@song.check_review_add_point(@current_api_user)
				hsh = {}
				hsh[:message] = "Review successfully added."
				render json: hsh
			rescue Exception => e
				puts "error_exception #{Time.now} #{e.message}"
				err_hash={}
				err_hash[:error]=e.message
				render :json => err_hash.to_json , status: :bad_request 
			end
		end

		def reviews
			begin
			rescue Exception => e
				puts "error_exception #{Time.now} #{e.message}"
				err_hash={}
				err_hash[:error]=e.message
				render :json => err_hash.to_json , status: :bad_request 
			end
		end
 #    def unread_notification
  
	# notification= @current_api_user.notifications.where(:read=>false).order("id desc")
	# @current_api_user.notifications.update(:read=>true)
	
	# hsh= {}
	#  hsh[:notification_count] = self.user.notifications.where(:read=>false).count
	
	# render :json => hsh
	# end
	private
	def set_song

		@song = Song.find(params[:song_id])
		
	end
	

end
