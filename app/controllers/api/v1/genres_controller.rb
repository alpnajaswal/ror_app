class Api::V1::GenresController < Api::V1::ApplicationController
	def index
			
			begin
				     @genre = Genre.paginate(:page => params[:page], :per_page => 6)
					 render :show_genre
				
		 	rescue Exception => e
		 		puts "error_exception #{Time.now} #{e.message}"
				err_hash={}
				err_hash[:error]=e.message
				render :json => err_hash.to_json  , status: :bad_request 
		 	end
		end

        def album
			
			begin
		album_ids =Album.joins(:songs).where('songs.is_submit'=>1).where("albums.genre_id"=>params[:genre_id]).map(&:id).uniq
		@album = Album.where(:genre_id=>params[:genre_id]).where(:id=>album_ids).order("id desc")
					 render :show_album
				
		 	rescue Exception => e
		 		puts "error_exception #{Time.now} #{e.message}"
				err_hash={}
				err_hash[:error]=e.message
				render :json => err_hash.to_json  , status: :bad_request 
		 	end
		end
end
