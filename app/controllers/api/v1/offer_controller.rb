class Api::V1::OfferController < Api::V1::ApplicationController
	def redeem_offer
		begin
		 @current_api_user.user_gift_requests.create(:offer_id=>params[:id])
		 
		 hhs={}
			hhs[:message]="Gift card request has been generated"
			render :json => hhs.to_json
         rescue Exception => e
			puts "error_exception #{Time.now} #{e.message}"
			err_hash={}
			err_hash[:error]=e.message
			render :json => err_hash.to_json  , status: :bad_request 
		end
	end
end
