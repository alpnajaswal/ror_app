class Api::V1::AlbumsController < Api::V1::ApplicationController
	before_action :authenticate_api_user , except: [:sign_up,:sign_in,:sign_up_social,:reset_password]
  def index
  	begin
  		 date =  Date.today
  		  date = date.strftime("%Y-%m-%d")
    #     days_before = (date.wday + 1) % 7 +1
    #      val = date.to_date - days_before
      
    #     last_date = val.strftime("%Y-%m-%d")
    album_ids =Album.joins(:songs).where('songs.is_submit'=>1).where("albums.genre_id"=>params[:genre_id]).map(&:id).uniq
    @albums = Album.where(:genre_id=>params[:genre_id]).where(:id => album_ids).order("id desc").paginate(:page => params[:page], :per_page => 6)
		if @albums.present?
          @album = @albums
		else
		 @album = @albums
          end  
         
            render :show_album
			rescue Exception => e
		 		puts "error_exception #{Time.now} #{e.message}"
				err_hash={}
				err_hash[:error]=e.message
				render :json => err_hash.to_json  , status: :bad_request 
		 	end
  end

   def show
   	
  	    begin
		         @album = Album.find_by_id(params[:id]) 
		        
				    # debugger
				     # @song = Song.where(:album_id=>params[:id])
					 render :album_song
				
		 	rescue Exception => e
		 		puts "error_exception #{Time.now} #{e.message}"
				err_hash={}
				err_hash[:error]=e.message
				render :json => err_hash.to_json  , status: :bad_request 
		 	end
  end
end
