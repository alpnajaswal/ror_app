class AddnotifiUserTable < ActiveRecord::Migration[5.1]
  def change
  	  	add_column :users,:profile_notification,:boolean,:default => true
  	  	add_column :users,:song_notification,:boolean,:default => true

  end
end
