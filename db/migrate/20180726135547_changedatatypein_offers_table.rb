class ChangedatatypeinOffersTable < ActiveRecord::Migration[5.1]
  def change
  	remove_column :offers, :offer_prize
  	 add_column :offers, :offer_prize, :integer
  end
end
