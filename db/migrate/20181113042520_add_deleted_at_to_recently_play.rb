class AddDeletedAtToRecentlyPlay < ActiveRecord::Migration[5.1]
  def change
    add_column :recently_plays, :deleted_at, :datetime
    add_index :recently_plays, :deleted_at
  end
end
