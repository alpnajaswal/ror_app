class CreateFavourites < ActiveRecord::Migration[5.1]
 
  def self.up
      create_table :favourites do |t|
        t.belongs_to :user
        t.belongs_to :favourable, :polymorphic => true
        t.timestamps
      end
    end

    def self.down
      drop_table :favourites
    end
end


