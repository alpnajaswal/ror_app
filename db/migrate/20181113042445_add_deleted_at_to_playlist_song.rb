class AddDeletedAtToPlaylistSong < ActiveRecord::Migration[5.1]
  def change
    add_column :playlist_songs, :deleted_at, :datetime
    add_index :playlist_songs, :deleted_at
  end
end
