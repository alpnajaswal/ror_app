class AddNotificationFieldstoUserTable < ActiveRecord::Migration[5.1]
  def change
  	add_column :users, :point_notification, :boolean , :default => true
  	add_column :users, :gift_notification, :boolean , :default => true
  end
end
