class CreatePointInfos < ActiveRecord::Migration[5.1]
  def change
    create_table :point_infos do |t|
      t.string :title
      t.string :description
      t.boolean :status

      t.timestamps
    end
  end
end
