class AddDeletedAtToFavourite < ActiveRecord::Migration[5.1]
  def change
    add_column :favourites, :deleted_at, :datetime
    add_index :favourites, :deleted_at
  end
end
