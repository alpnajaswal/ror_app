class Changeclimntosoiallinktable < ActiveRecord::Migration[5.1]
  def change
  	rename_column :user_social_links, :type, :url_type
  end
end
