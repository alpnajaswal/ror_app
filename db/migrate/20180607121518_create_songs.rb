class CreateSongs < ActiveRecord::Migration[5.1]
  def change
    create_table :songs do |t|
      t.integer :album_id ,:default => ""
      t.string :title ,:default => ""
      t.string :file ,:default => ""
      t.string :description ,:default => ""
      t.references :album, foreign_key: true

      t.timestamps
    end
  end
end
