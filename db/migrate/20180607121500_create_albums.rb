class CreateAlbums < ActiveRecord::Migration[5.1]
  def change
    create_table :albums do |t|
      t.string :title ,:default => ""
      t.integer :user_id ,:default => ""
      t.integer :gentre_id ,:default => ""
      t.string :image ,:default => ""
      t.string :description ,:default => ""
      t.boolean :status ,:default =>true
      t.references :user, foreign_key: true
      t.references :genre, foreign_key: true

      t.timestamps
    end
  end
end
