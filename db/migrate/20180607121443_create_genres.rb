class CreateGenres < ActiveRecord::Migration[5.1]
  def change
    create_table :genres do |t|
      t.string :name ,:default => ""
      t.string :image ,:default => ""
      t.boolean :status ,:default => true

      t.timestamps
    end
  end
end
