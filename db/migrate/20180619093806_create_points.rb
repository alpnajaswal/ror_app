class CreatePoints < ActiveRecord::Migration[5.1]
  def change
    create_table :points do |t|
      t.references :user, foreign_key: true
       t.integer :points,:default=>""
      t.string :point_type,:default=>""
      t.boolean :redeem,:default=>false
      t.timestamps
    end
  end
end
