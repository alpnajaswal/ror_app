class AddFieldsToUserTable < ActiveRecord::Migration[5.1]
  def change
  	add_column :users, :first_name, :string ,:default => ""
  	add_column :users, :last_name, :string ,:default => ""
	add_column :users, :admin, :boolean , :default => false
	add_column :users, :role, :string,:default => "U"
	add_column :users, :gender,:string,:default => ""
	add_column :users, :mobile, :bigint , :default => ""
	
	add_column :users, :city, :string , :default => ""
    add_column :users, :state, :string , :default => ""
	add_column :users, :status,:boolean, :default => true
  end
end
