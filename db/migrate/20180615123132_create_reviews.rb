class CreateReviews < ActiveRecord::Migration[5.1]
  def self.up
      create_table :reviews do |t|
        t.belongs_to :reviewer
        t.belongs_to :reviewable, :polymorphic => true
        t.string :comment
        t.timestamps
      end
    end

    def self.down
      drop_table :reviews
    end
end


