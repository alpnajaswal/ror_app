class CreateOffers < ActiveRecord::Migration[5.1]
  def change
    create_table :offers do |t|
      t.string :title,:default=>""
      t.string :description,:default=>""
      t.string :offer_prize,:default=>""
      t.string :offer_type,:default=>""
      t.boolean :status,:default=>false

      t.timestamps
    end
  end
end
