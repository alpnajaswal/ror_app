class CreatePlaylistSongs < ActiveRecord::Migration[5.1]
  def change
    create_table :playlist_songs do |t|
      t.integer :playlist_id ,:default => ""
      t.string :song_id ,:default => ""
      t.references :user_playlist, foreign_key: true
      t.references :song, foreign_key: true

      t.timestamps
    end
  end
end
