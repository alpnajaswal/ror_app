class CreateRecentlyPlays < ActiveRecord::Migration[5.1]
  def change
    create_table :recently_plays do |t|
      t.integer :user_id ,:default => ""
      t.string :song_id ,:default => ""
      t.references :user, foreign_key: true
      t.references :song, foreign_key: true

      t.timestamps
    end
  end
end
