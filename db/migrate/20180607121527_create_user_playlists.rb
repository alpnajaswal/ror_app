class CreateUserPlaylists < ActiveRecord::Migration[5.1]
  def change
    create_table :user_playlists do |t|
      t.integer :user_id ,:default => ""
      t.string :name ,:default => ""
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
