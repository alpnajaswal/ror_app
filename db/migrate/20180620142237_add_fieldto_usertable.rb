class AddFieldtoUsertable < ActiveRecord::Migration[5.1]
  def change
  	remove_column :users, :gender
  	add_column :users, :age,:integer,:default => ""
  	add_column :users, :dob,:date,:default => ""
  end
end
