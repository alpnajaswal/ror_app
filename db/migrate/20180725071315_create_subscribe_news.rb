class CreateSubscribeNews < ActiveRecord::Migration[5.1]
  def change
    create_table :subscribe_news do |t|
      t.string :email

      t.timestamps
    end
  end
end
