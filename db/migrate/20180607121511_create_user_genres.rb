class CreateUserGenres < ActiveRecord::Migration[5.1]
  def change
    create_table :user_genres do |t|
      t.integer :user_id ,:default => ""
      t.integer :gentre_id ,:default => ""
      t.references :user, foreign_key: true
      t.references :genre, foreign_key: true

      t.timestamps
    end
  end
end
