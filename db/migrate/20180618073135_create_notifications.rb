class CreateNotifications < ActiveRecord::Migration[5.1]
  def change
    create_table :notifications do |t|
      t.string :notification_type,:default=>""
      t.integer :notification_type_id
      t.string :title,:default=>""
      t.string :message,:default=>""
      t.integer :sender_id
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
