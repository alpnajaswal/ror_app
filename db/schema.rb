# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20181119074909) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "albums", force: :cascade do |t|
    t.string "title", default: ""
    t.bigint "user_id"
    t.string "image", default: ""
    t.string "description", default: ""
    t.boolean "status", default: true
    t.bigint "genre_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.date "release_date"
    t.index ["genre_id"], name: "index_albums_on_genre_id"
    t.index ["user_id"], name: "index_albums_on_user_id"
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer "priority", default: 0, null: false
    t.integer "attempts", default: 0, null: false
    t.text "handler", null: false
    t.text "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string "locked_by"
    t.string "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["priority", "run_at"], name: "delayed_jobs_priority"
  end

  create_table "favourites", force: :cascade do |t|
    t.bigint "user_id"
    t.string "favourable_type"
    t.bigint "favourable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_favourites_on_deleted_at"
    t.index ["favourable_type", "favourable_id"], name: "index_favourites_on_favourable_type_and_favourable_id"
    t.index ["user_id"], name: "index_favourites_on_user_id"
  end

  create_table "follows", force: :cascade do |t|
    t.string "followable_type", null: false
    t.bigint "followable_id", null: false
    t.string "follower_type", null: false
    t.bigint "follower_id", null: false
    t.boolean "blocked", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["followable_id", "followable_type"], name: "fk_followables"
    t.index ["followable_type", "followable_id"], name: "index_follows_on_followable_type_and_followable_id"
    t.index ["follower_id", "follower_type"], name: "fk_follows"
    t.index ["follower_type", "follower_id"], name: "index_follows_on_follower_type_and_follower_id"
  end

  create_table "genres", force: :cascade do |t|
    t.string "name", default: ""
    t.string "image", default: ""
    t.boolean "status", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "icon_image"
  end

  create_table "notifications", force: :cascade do |t|
    t.string "notification_type", default: ""
    t.integer "notification_type_id"
    t.string "title", default: ""
    t.string "message", default: ""
    t.integer "sender_id"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "read", default: false
    t.index ["user_id"], name: "index_notifications_on_user_id"
  end

  create_table "offers", force: :cascade do |t|
    t.string "title", default: ""
    t.string "description", default: ""
    t.string "offer_type", default: ""
    t.boolean "status", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "points"
    t.decimal "offer_prize"
  end

  create_table "playlist_songs", force: :cascade do |t|
    t.integer "playlist_id"
    t.bigint "song_id"
    t.bigint "user_playlist_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_playlist_songs_on_deleted_at"
    t.index ["song_id"], name: "index_playlist_songs_on_song_id"
    t.index ["user_playlist_id"], name: "index_playlist_songs_on_user_playlist_id"
  end

  create_table "point_infos", force: :cascade do |t|
    t.string "title"
    t.string "description"
    t.boolean "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "points", force: :cascade do |t|
    t.bigint "user_id"
    t.integer "points"
    t.string "point_type", default: ""
    t.boolean "redeem", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_points_on_user_id"
  end

  create_table "recently_plays", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "song_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "full_listen", default: false
    t.boolean "redeem_point", default: false
    t.string "point_type"
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_recently_plays_on_deleted_at"
    t.index ["song_id"], name: "index_recently_plays_on_song_id"
    t.index ["user_id"], name: "index_recently_plays_on_user_id"
  end

  create_table "reviews", force: :cascade do |t|
    t.bigint "reviewer_id"
    t.string "reviewable_type"
    t.bigint "reviewable_id"
    t.string "comment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "redeem_point", default: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_reviews_on_deleted_at"
    t.index ["reviewable_type", "reviewable_id"], name: "index_reviews_on_reviewable_type_and_reviewable_id"
    t.index ["reviewer_id"], name: "index_reviews_on_reviewer_id"
  end

  create_table "songs", force: :cascade do |t|
    t.bigint "album_id"
    t.string "title", default: ""
    t.string "file", default: ""
    t.string "description", default: ""
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "view_count", default: 0
    t.integer "duration"
    t.integer "is_submit", default: 0
    t.datetime "deleted_at"
    t.index ["album_id"], name: "index_songs_on_album_id"
    t.index ["deleted_at"], name: "index_songs_on_deleted_at"
  end

  create_table "subscribe_news", force: :cascade do |t|
    t.string "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_genres", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "genre_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["genre_id"], name: "index_user_genres_on_genre_id"
    t.index ["user_id"], name: "index_user_genres_on_user_id"
  end

  create_table "user_gift_requests", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "offer_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "status", default: false
    t.index ["offer_id"], name: "index_user_gift_requests_on_offer_id"
    t.index ["user_id"], name: "index_user_gift_requests_on_user_id"
  end

  create_table "user_playlists", force: :cascade do |t|
    t.bigint "user_id"
    t.string "name", default: ""
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_user_playlists_on_user_id"
  end

  create_table "user_social_links", force: :cascade do |t|
    t.string "url_type"
    t.string "url"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_user_social_links_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "device_id", default: ""
    t.string "device_type", default: ""
    t.string "image", default: ""
    t.boolean "notification_toogle", default: true
    t.string "phone_number", default: ""
    t.string "api_secret", default: ""
    t.boolean "phone_verified", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "first_name", default: ""
    t.string "last_name", default: ""
    t.boolean "admin", default: false
    t.string "role", default: "U"
    t.bigint "mobile"
    t.string "city", default: ""
    t.string "state", default: ""
    t.boolean "status", default: true
    t.boolean "point_notification", default: true
    t.boolean "gift_notification", default: true
    t.integer "age"
    t.date "dob"
    t.string "fp_token"
    t.datetime "fp_token_expiry"
    t.string "otp"
    t.datetime "otp_expiry"
    t.integer "total_points"
    t.boolean "profile_notification", default: true
    t.boolean "song_notification", default: true
    t.string "social_id", default: ""
    t.string "username", default: ""
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "albums", "genres"
  add_foreign_key "albums", "users"
  add_foreign_key "notifications", "users"
  add_foreign_key "playlist_songs", "songs"
  add_foreign_key "playlist_songs", "user_playlists"
  add_foreign_key "points", "users"
  add_foreign_key "recently_plays", "songs"
  add_foreign_key "recently_plays", "users"
  add_foreign_key "songs", "albums"
  add_foreign_key "user_genres", "genres"
  add_foreign_key "user_genres", "users"
  add_foreign_key "user_gift_requests", "offers"
  add_foreign_key "user_gift_requests", "users"
  add_foreign_key "user_playlists", "users"
  add_foreign_key "user_social_links", "users"
end
