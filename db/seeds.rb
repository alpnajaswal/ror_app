# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

   puts "seed start"
 User.create(:email=>"admin@admin.com",:password=>"netset@123",:admin=>true)
 Genre.delay(queue: "Job", priority: 5, run_at: 2.hours.from_now).upload_all_genre_song
 # Genre.create(:name=>"POP",:image => File.open('/home/netset/apps/rails/wave_music/app/assets/images/man.png'))
 # Album.create(:title=>"POP HIT",:user_id=>5,:genre_id=>1,:description=>"This is first album",:image => File.open('/home/netset/apps/rails/wave_music/app/assets/images/man.png'))
 
 # Song.create(:album_id=>1,:title=>"Song1",:description=>"This is first song",:file => File.open('/home/netset/apps/rails/wave_music/app/assets/songs/song1.mp3'))
 # Song.create(:album_id=>2,:title=>"Song1",:description=>"This is first song",:file => File.open('/home/netset/apps/rails/wave_music/app/assets/songs/song1.mp3'))
 # Song.create(:album_id=>1,:title=>"Song2",:description=>"This is second song",:file => File.open('/home/netset/apps/rails/wave_music/app/assets/songs/song2.mp3'))
 # Song.create(:album_id=>2,:title=>"Song2",:description=>"This is second song",:file => File.open('/home/netset/apps/rails/wave_music/app/assets/songs/song2.mp3'))
 
puts "seed end"